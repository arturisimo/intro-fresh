#!/bin/bash

header="Private-Token: $CI_GITLAB_TOKEN"
first_commit="5aedc78b579bd8046076d197140ef2c8283b7934"

declare -a tags

# get all tags
readarray -t tags < <(curl -s --location "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/tags?per_page=100" --header "$header" | jq -r '.[].name')

idx=1 
changeset="---\ntitle: Changeset\nlayout: base.njk\n---\n# Changeset\n\n"

for tag in "${tags[@]}"; do
    from=${tags[idx]}

    if [[ -z $from ]]; then
        from=$first_commit
    fi
    
    # get the commits in actual tag
    url_compare="https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/compare?from=$from&to=$tag"
    jq_expression='.commits[] | select(.title | startswith("Merge") | not) | "- [\(.title)](\(.web_url))\n"'
    commits=$(curl -s --location "$url_compare" --header "$header")
    # format
    commits_md=$(echo "$commits" | jq -r "$jq_expression")
    commits_md_with_breaks=$(echo -e "$commits_md")
    
    changeset+="## Changeset $tag\n"
    changeset+="$commits_md_with_breaks\n\n"

    ((idx++))
done

# Build changeset file
{ echo -e "$changeset"; } > "$CI_PROJECT_DIR/doc/index.md"
