import type { Post } from "../types.d.ts";
import { extract } from "$std/encoding/front_matter/any.ts";
import { render } from "$gfm/mod.ts";

export async function loadDoc(slug: string): Promise<Post | null> {
  const raw = await Deno
    .readTextFile(`./content/docs/${slug}.md`)
    .catch(() => null);

  if (!raw) return null;
  const { attrs, body } = extract(raw);
  const params = attrs as Record<string, string>;
  const doc: Post = {
    slug,
    title: params.title,
    body: render(body),
    date: new Date(params.date),
    excerpt: params.excerpt,
  };
  return doc;
}

export async function listDocs(): Promise<Post[]> {
  const promises = [];
  for await (const entry of Deno.readDir("./content/docs")) {
    const { name } = entry;
    const [slug] = name.split(".");
    if (slug) promises.push(loadDoc(slug));
  }

  const docs = await Promise.all(promises) as Post[];

  docs.sort((a, b) => {
    return b.date.getTime() - a.date.getTime(); // DESC
  });

  return docs;
}
