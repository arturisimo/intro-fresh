import type { Post } from "../types.d.ts";
import { render } from "$gfm/mod.ts";

export async function loadPost(slug: string): Promise<Post | null> {
  const kv = await Deno.openKv();
  const entry = await kv.get(["posts", slug]);
  const post: Post = entry.value as Post;
  return {
    slug,
    title: post.title,
    body: render(post.body),
    date: new Date(post.date),
    excerpt: post.excerpt,
  };
}

export async function listsPosts(): Promise<Post[]> {
  const kv = await Deno.openKv();
  const entries: Promise<Post[]> = kv.list({ prefix: ["posts"] });

  const posts = [];

  for await (const entry of entries) {
    const post = entry.value as Post;
    posts.push(post);
  }

  posts.sort((a, b) => {
    return b.date.getTime() - a.date.getTime(); // DESC
  });

  return posts;
}
