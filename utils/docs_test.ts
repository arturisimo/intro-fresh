import { loadDoc } from "./docs.ts";
import { assertEquals } from "$std/testing/asserts.ts";

Deno.test("loadDoc() returns null if the post does not exist", async () => {
  const post = await loadDoc("non-existent");
  assertEquals(post, null);
});

Deno.test("loadDoc() returns a post object if post does exist", async () => {
  const doc = await loadDoc("intro-deno");
  assertEquals(doc?.slug, "intro-deno");
  assertEquals(doc?.title, "Introduction to deno");
});
