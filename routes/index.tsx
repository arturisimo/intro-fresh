import { Handlers, PageProps } from "$fresh/server.ts";
import { Post } from "../types.d.ts";
import { listsPosts } from "../utils/posts.ts";

export const handler: Handlers = {
  async GET(req, context) {
    const posts = await listsPosts();
    return context.render({ posts });
  },
};

export default function Home(props: PageProps) {
  const { data } = props;
  const { posts } = data;

  return (
    <>
      <div class="px-4 py-8 mx-auto">
        <div class="max-w-screen-md mx-auto flex flex-col items-center justify-center">
          <img
            class="my-6"
            src="/logo.svg"
            width="128"
            height="128"
            alt="the Fresh logo: a sliced lemon dripping with juice"
          />
          <main class="p-4">
            <h1 class="text-4xl font-bold">Simple blog</h1>

            {posts.map((post: Post) => (
              <article class="p-4">
                <h2 class="text-2xl">
                  <a class="hover:text-blue-600" href={`/blog/${post.slug}`}>
                    {post.title}
                  </a>
                </h2>
                <time>{Intl.DateTimeFormat("es").format(post.date)}</time>
              </article>
            ))}
          </main>
        </div>
      </div>
    </>
  );
}
