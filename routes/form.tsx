import { Head } from "$fresh/runtime.ts";
import type { Post } from "../types.d.ts";

function createSlug(title: string): string {
  // Remove special characters and spaces, and convert to lowercase
  const slug = title
    .trim()
    .toLowerCase()
    .replace(/[^a-zA-Z0-9 -]/g, "")
    .replace(/\s+/g, "-")
    .replace(/-+/g, "-");

  return slug;
}

export const handler: Handlers = {
  async GET(request, context) {
    return await context.render();
  },
  async POST(req, ctx) {
    const form = await req.formData();
    const title = form.get("title")?.toString();
    const slug = createSlug(title);

    const post: Post = {
      slug: slug,
      title: title,
      body: form.get("content")?.toString(),
      date: new Date(),
      excerpt: undefined,
    };
    const kv = await Deno.openKv();
    const result = await kv.set(["posts", slug], post);

    const headers = new Headers();
    headers.set("location", "/");
    return new Response(null, {
      status: 303, // See Other
      headers,
    });
  },
};

export default function form() {
  return (
    <>
      <Head>
        <title>Add post</title>
      </Head>
      <div class="px-4 py-8 mx-auto">
        <div class="max-w-screen-md mx-auto flex flex-col items-center justify-center">
          <header>
            <h1 class="text-4xl font-bold">New post</h1>
          </header>

          <article class="p-4">
            <form
              method="POST"
              class="bg-white p-6 rounded-lg shadow-md"
            >
              <div class="mb-4">
                <label for="title" class="block text-gray-700 font-medium">
                  Title
                </label>
                <input
                  type="text"
                  name="title"
                  id="title"
                  class="w-full py-2 px-3 border rounded-lg"
                >
                </input>
              </div>

              <div class="mb-4">
                <label for="content" class="block text-gray-700 font-medium">
                  Content
                </label>
                <textarea
                  name="content"
                  id="content"
                  class="w-full py-2 px-3 border rounded-lg"
                  rows="6"
                >
                </textarea>
              </div>

              <div class="mb-4">
                <button
                  type="submit"
                  class="bg-blue-500 text-white px-4 py-2 rounded-full hover:bg-blue-600 transition duration-200"
                >
                  Submit
                </button>
              </div>
            </form>
          </article>
        </div>
      </div>
    </>
  );
}
