import { Handlers, PageProps } from "$fresh/server.ts";
import { loadPost } from "../../utils/posts.ts";
import { CSS } from "$gfm/mod.ts";
import { Head } from "$fresh/runtime.ts";
import Button from "../../islands/Button.tsx";

export const handler: Handlers = {
  async GET(request, context) {
    const { slug } = context.params;
    const post = await loadPost(slug);
    return context.render({ post });
  },
};

export default function PagePost(props: PageProps) {
  const { post } = props?.data || {};
  return (
    <>
      <Head>
        <title>{post.title}</title>
      </Head>
      <div class="px-4 py-8 mx-auto">
        <div class="max-w-screen-md mx-auto flex flex-col items-center justify-center">
          <img
            class="my-6"
            src="/logo.svg"
            width="128"
            height="128"
            alt="the Fresh logo: a sliced lemon dripping with juice"
          />
          <header>
            <h1 class="text-4xl font-bold">{post.title}</h1>
            <Button />
            <time>{Intl.DateTimeFormat("es").format(post.date)}</time>
          </header>

          <article class="p-4">
            <style dangerouslySetInnerHTML={{ __html: CSS }} />
            <div
              class="markdown-body"
              dangerouslySetInnerHTML={{ __html: post.body }}
            />
          </article>
        </div>
      </div>
    </>
  );
}
