import { Handlers, PageProps } from "$fresh/server.ts";
import { listDocs } from "../utils/docs.ts";
import { Post } from "../types.d.ts";

export const handler: Handlers = {
  async GET(request, context) {
    const docs = await listDocs();
    return context.render({ docs });
  },
};

export default function PageDoc(props: PageProps) {
  const { data } = props;
  const { docs } = data;

  return (
    <>
      <div class="px-4 py-8 mx-auto">
        <div class="max-w-screen-md mx-auto flex flex-col items-center justify-center">
          <img
            class="my-6"
            src="/logo.svg"
            width="128"
            height="128"
            alt="the Fresh logo: a sliced lemon dripping with juice"
          />
          <main class="p-4">
            <h1 class="text-4xl font-bold">Some docs</h1>

            {docs.map((doc: Post) => (
              <article class="p-4">
                <h2 class="text-2xl">
                  <a class="hover:text-blue-600" href={`/doc/${doc.slug}`}>
                    {doc.title}
                  </a>
                </h2>
                <time>{Intl.DateTimeFormat("es").format(doc.date)}</time>
              </article>
            ))}
          </main>
        </div>
      </div>
    </>
  );
}
