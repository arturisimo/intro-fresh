---
title: Introduction to deno
date: 2023-10-22
excerpt: introduction to deno
---

# Deno

A runtime for JS an TS

- Secure by default
- Native support for TypeScript and JSX
- Ships only a single executable file
- Testing, linting, formatting, and more out of the box
- High performance async I/O with Rust and Tokio
- Backwards compatible with Node.js and npm

https://docs.deno.com/

Version `deno --version`

Run script `deno run .\main.ts`

Run script allow all net permissions `deno run -A .\main.ts`

Run HTTP server on localhost:8000:

```ts
Deno.serve((_request: Request) => {
  return new Response("Hello, world!");
});
```

## Test

```ts
import { assertEquals } from "$std/assert/mod.ts";
import Person, { sayHello } from "./person.ts";

Deno.test("sayHello: assert response from method", () => {
  const grace: Person = {
    lastName: "Hopper",
    firstName: "Grace",
  };

  assertEquals("Hello, Grace Hopper!", sayHello(grace));
});
```

## Standard Library

standard modules set of code to be used fearlessly.

https://www.deno.land/std

## Deno compile

Compile script into a self-contained executable:
`deno compile [--output <OUT>] <SRC>`

https://medium.com/deno-the-complete-reference/distribute-application-with-deno-70bb05034507
