---
title: Deploy fresh app in gitlab
date: 2023-10-22
excerpt: deploy fresh app
---

## Deno deploy
"Serverless JavaScript hosting with zero config, worldwide"

Deno Deploy is a globally distributed platform for serverless JavaScript applications. The code runs on managed servers geographically close to your users, enabling low latency and faster response times. Deploy applications run on fast, light-weight V8 isolates rather than virtual machines, powered by the Deno runtime.

https://deno.com/deploy

**Edge computing** is a distributed computing paradigm where computation is performed closer to the data source or end-users. It involves processing data and running applications in the **edge location**: as close as possible to the location where data is generated or consumed.

Deno deploy allow to deploy serverless functions to edge locations improving responsiveness for applications that require low-latency processing.

![edge location](/edge_location.png)

### deploy using deployctl

https://docs.deno.com/deploy/manual/deployctl

In https://dash.deno.com create project and access token

Deno deploy
`deployctl deploy --project <project> ./main.ts --prod --token <token>`


## gitlab runner register

installation gitlab runner:
https://docs.gitlab.com/runner/install/linux-repository.html

installation on ubuntu

```
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner
```

check status `sudo gitlab-runner status`

register sudo `gitlab-runner register`

list configured runners `sudo gitlab-runner list`

restart `sudo gitlab-runner restart`

Docker-in-Docker requires privileged mode to function in
/etc/gitlab-runner/config.toml
