---
title: Introduction to fresh
date: 2023-10-22
excerpt: introduction to deno
---

# Fresh

Deno-native frameworks: `deno run -A -r https://fresh.deno.dev`

Fresh is a full stack modern web framework for JavaScript and TypeScript.

At its core, Fresh is a combination of a routing framework and templating engine
that renders just-in-time pages on demand.

The framework uses Preact and JSX for rendering and templating on both the
server and the client.

No JavaScript send to clients by default. The majority of rendering is done on a
server, and the client is only responsible for re-rendering small **islands of
interactivity**

run app `deno task start`

https://fresh.deno.dev/docs/getting-started

## Preact

[Preact](https://preactjs.com) is a Fast 3kB alternative to React with the same modern API

### [tutorial](https://preactjs.com/tutorial)

Preact provide a way to construct **virtual DOM**, the DOM tree is updated to match the structure described by the Virtual DOM tree.

Using the virtual DOM allows to compose the UI declaratively, allows to describe what the DOM should look like after a event received

```js
import { createElement, render } from 'preact';

let vdom = createElement(
  'p',              // a <p> element
  { class: 'big' }, // with class="big"
  'Hello World!'    // and the text "Hello World!"
);

render(vdom, document.body);
```


## Edge runtime
